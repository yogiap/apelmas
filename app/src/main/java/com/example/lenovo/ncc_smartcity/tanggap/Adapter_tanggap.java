package com.example.lenovo.ncc_smartcity.tanggap;

import android.Manifest;
import android.content.Context;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.tanggap.tanggap_model.List_Post;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by arimahardika on 14/03/2018.
 */

public class Adapter_tanggap extends RecyclerView.Adapter<Adapter_tanggap.TanggapViewHolder> {

    private List<List_Post> listTanggap;
    private Context context;
    private int type_template;
    private String BASE_URL = URLCollection.DATA_SOURCE_LOKER;

    private String TAG = "Adapter_tanggap";

    public Adapter_tanggap(List<List_Post> listTanggap, Context context, int type) {
        this.listTanggap = listTanggap;
        this.context = context;
        this.type_template = type;
    }

    static public void shareImage(String url, final String teks, final Context context) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_TEXT, teks);
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    static public Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {

            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_.png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    public TanggapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tanggap_cardview_new, parent, false);
        return new TanggapViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TanggapViewHolder holder, final int position) {
        List_Post model_tanggap = listTanggap.get(position);

        String getProfilePicURL = model_tanggap.getProfile_pic_url();
        String getUsername = model_tanggap.getUsername();
        String getDate = model_tanggap.getEvent_tgl();
        String getTime = model_tanggap.getEvent_time();
        final String getEventPhoto = new URLCollection().BASE_URL_FOTO + model_tanggap.getEvent_photo_url();
        final String getEventLocation = model_tanggap.getEvent_location();
        String getEventStatus = model_tanggap.getEvent_status();
        final String getEventTitle = model_tanggap.getEvent_category();
        final String getEventDescription = model_tanggap.getEvent_description();
        //try{
        Log.e(TAG, "onBindViewHolder: " + getEventPhoto);
//            InformationLocation informationLocation = new InformationLocation(getEventLocation);
//            String getEventAddress = informationLocation.getCompleteAddressString();
//        }catch (Exception e){
//            Log.e(TAG, "onBindViewHolder: "+e );
//        }

        Log.e(TAG, "onBindViewHolder: " +
                "--" + getProfilePicURL +
                "--" + getUsername +
                "--" + getDate +
                "--" + getTime +
                "--" + getEventPhoto +
                "--" + getEventLocation +
                "--" + getEventStatus +
                "--" + getEventTitle +
                "--" + getEventDescription);

//        Picasso.with(context).load(getProfilePicURL).into(holder.circle_image_view_profilePic);

        holder.txt_username.setText(getUsername);
        holder.txt_date.setText(getDate);

        Picasso.with(context).load(getEventPhoto).into(holder.image_eventPhoto);

        holder.txt_eventLocation.setText(getEventLocation);
        holder.txt_eventLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("google.navigation:q=" + getEventLocation);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                //intent.setPackage("com.google.android.app.maps");
                view.getContext().startActivity(intent);
            }
        });

        //0 = not process, 1 = on process, 2 = done
        Log.d("ColorChange", "onBindViewHolder: eventStatus : " + getEventStatus);
        switch (Integer.valueOf(getEventStatus)) {
            case 0:
                holder.circle_image_view_status.setImageResource(R.drawable.belum);
                break;
            case 1:
                holder.circle_image_view_status.setImageResource(R.drawable.sedang);
                break;
            case 2:
                holder.circle_image_view_status.setImageResource(R.drawable.sudah);
                break;
        }

        holder.txt_eventDescription.setText(getEventDescription);

        holder.btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("google.navigation:q=" + getEventLocation);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                //intent.setPackage("com.google.android.app.maps");
                view.getContext().startActivity(intent);
            }
        });
        holder.btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareImage(getEventPhoto, getEventLocation
                        + "\n" + getEventTitle + "\n" + getEventDescription, view.getContext());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTanggap.size();
    }

    public class TanggapViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_date, txt_eventLocation, txt_eventTitle, txt_eventDescription, txt_tlp;
        ImageView image_eventPhoto;
        CircleImageView circle_image_view_profilePic, circle_image_view_status;

        Button btn_location, btn_share;

        public TanggapViewHolder(View itemView) {
            super(itemView);

            circle_image_view_profilePic = itemView.findViewById(R.id.tanggapNewLayout_civ_profilePicture);
            txt_username = itemView.findViewById(R.id.tanggapNewLayout_tv_username);
            txt_date = itemView.findViewById(R.id.tanggapNewLayout_tv_date);
            circle_image_view_status = itemView.findViewById(R.id.tanggapNewLayout_civ_indicator);

            image_eventPhoto = itemView.findViewById(R.id.tanggapNewLayout_img_event);
            txt_eventLocation = itemView.findViewById(R.id.tanggapNewLayout_tv_address);
            txt_eventDescription = itemView.findViewById(R.id.tanggapNewLayout_tv_description);

            btn_location = itemView.findViewById(R.id.tanggapNewLayout_btn_location);
            btn_share = itemView.findViewById(R.id.tanggapNewLayout_btn_share);

        }
    }
}
