package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 05/06/2018.
 */

public class ListIdrekan {

    @SerializedName("id_lowongan")
    String id_lowongan;

    @SerializedName("id_perusahaan")
    String id_perusahaan;

    @SerializedName("posisi_lowongan")
    String posisi_lowongan;

    @SerializedName("id_bidang")
    String id_bidang;

    @SerializedName("syarat_umum")
    String syarat_umum;

    @SerializedName("syarat_umur")
    String syarat_umur;

    @SerializedName("pendidikan_jenjang")
    String pendidikan_jenjang;

    @SerializedName("pendidikan_prodi")
    String pendidikan_prodi;

    @SerializedName("syarat_skill")
    String syarat_skill;

    @SerializedName("deskripsi_pekerjaan")
    String deskripsi_pekerjaan;

    @SerializedName("gaji")
    String gaji;

    @SerializedName("cara_apply")
    String cara_apply;

    @SerializedName("syarat_dokumen")
    String syarat_dokumen;

    @SerializedName("tgl_berakhir")
    String tgl_berakhir;

    @SerializedName("nama_perusahaan")
    String nama_perusahaan;

    @SerializedName("deskripsi_perusahaan")
    String deskripsi_perusahaan;

    @SerializedName("alamat_perusahaan")
    String alamat_perusahaan;

    @SerializedName("bidang_perusahaan")
    String bidang_perusahaan;

    @SerializedName("url_content")
    String url_content;

    @SerializedName("id_category")
    String id_category;

    @SerializedName("nama_category")
    String nama_category;

    @SerializedName("nama_bidang")
    String nama_bidang;

    public String getId_lowongan() {
        return id_lowongan;
    }

    public void setId_lowongan(String id_lowongan) {
        this.id_lowongan = id_lowongan;
    }

    public String getPosisi_lowongan() {
        return posisi_lowongan;
    }

    public void setPosisi_lowongan(String posisi_lowongan) {
        this.posisi_lowongan = posisi_lowongan;
    }

    public String getId_perusahaan() {
        return id_perusahaan;
    }

    public void setId_perusahaan(String id_perusahaan) {
        this.id_perusahaan = id_perusahaan;
    }

    public String getId_bidang() {
        return id_bidang;
    }

    public void setId_bidang(String id_bidang) {
        this.id_bidang = id_bidang;
    }

    public String getSyarat_umum() {
        return syarat_umum;
    }

    public void setSyarat_umum(String syarat_umum) {
        this.syarat_umum = syarat_umum;
    }

    public String getSyarat_umur() {
        return syarat_umur;
    }

    public void setSyarat_umur(String syarat_umur) {
        this.syarat_umur = syarat_umur;
    }

    public String getPendidikan_jenajang() {
        return pendidikan_jenjang;
    }

    public void setPendidikan_jenajang(String pendidikan_jenajang) {
        this.pendidikan_jenjang = pendidikan_jenajang;
    }

    public String getPedidikan_prodi() {
        return pendidikan_prodi;
    }

    public void setPedidikan_prodi(String pedidikan_prodi) {
        this.pendidikan_prodi = pedidikan_prodi;
    }

    public String getSyarat_skill() {
        return syarat_skill;
    }

    public void setSyarat_skill(String syarat_skill) {
        this.syarat_skill = syarat_skill;
    }

    public String getDeskripsi_pekerjaan() {
        return deskripsi_pekerjaan;
    }

    public void setDeskripsi_pekerjaan(String deskripsi_pekerjaan) {
        this.deskripsi_pekerjaan = deskripsi_pekerjaan;
    }

    public String getGaji() {
        return gaji;
    }

    public void setGaji(String gaji) {
        this.gaji = gaji;
    }

    public String getCara_apply() {
        return cara_apply;
    }

    public void setCara_apply(String cara_apply) {
        this.cara_apply = cara_apply;
    }

    public String getSyarat_dokumen() {
        return syarat_dokumen;
    }

    public void setSyarat_dokumen(String syarat_dokumen) {
        this.syarat_dokumen = syarat_dokumen;
    }

    public String getTgl_berakhir() {
        return tgl_berakhir;
    }

    public void setTgl_berakhir(String tgl_berakhir) {
        this.tgl_berakhir = tgl_berakhir;
    }

    public String getNama_perusahaan() {
        return nama_perusahaan;
    }

    public void setNama_perusahaan(String nama_perusahaan) {
        this.nama_perusahaan = nama_perusahaan;
    }

    public String getDeskripsi_perusahaan() {
        return deskripsi_perusahaan;
    }

    public void setDeskripsi_perusahaan(String deskripsi_perusahaan) {
        this.deskripsi_perusahaan = deskripsi_perusahaan;
    }

    public String getAlamat_perusahaan() {
        return alamat_perusahaan;
    }

    public void setAlamat_perusahaan(String alamat_perusahaan) {
        this.alamat_perusahaan = alamat_perusahaan;
    }

    public String getBidang_perusahaan() {
        return bidang_perusahaan;
    }

    public void setBidang_perusahaan(String bidang_perusahaan) {
        this.bidang_perusahaan = bidang_perusahaan;
    }

    public String getUrl_content() {
        return url_content;
    }

    public void setUrl_content(String url_content) {
        this.url_content = url_content;
    }

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getNama_category() {
        return nama_category;
    }

    public void setNama_category(String nama_category) {
        this.nama_category = nama_category;
    }

    public String getNama_bidang() {
        return nama_bidang;
    }

    public void setNama_bidang(String nama_bidang) {
        this.nama_bidang = nama_bidang;
    }
}
