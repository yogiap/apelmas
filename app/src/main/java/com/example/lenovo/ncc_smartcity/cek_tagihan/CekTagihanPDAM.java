package com.example.lenovo.ncc_smartcity.cek_tagihan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataTagihan;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CekTagihanPDAM extends AppCompatActivity {
    MessageVariabe messageVariabe = new MessageVariabe();
    String TAG = "CekTagihanPDAM",
            BASE_URL = URLCollection.BASE_URL_NCC,
            TOKEN_MOBILE = "X00W";

    Call<ResponseBody> get_data;
    SendDataTagihan base_url_management;

    CheckConn checkConn = new CheckConn();

    Button btn_hapus, btn_cekTagihan;

    LinearLayout ll_hasilCekTagihan;

    EditText et_nomorTagihan;

    Base_url base_url;

    TextView tvNama, tvAlamat, tvNosal, tvGolongan, tvTagihan, tvStatus;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_tagihan_pdam);

        et_nomorTagihan = findViewById(R.id.et_nomorTagihanPDAM);

        ll_hasilCekTagihan = findViewById(R.id.ll_cekTagihanHasil);

        tvNama = (TextView) findViewById(R.id.tv_nama);
        tvAlamat = (TextView) findViewById(R.id.tv_alamat);
        tvNosal = (TextView) findViewById(R.id.tv_nosal);
        tvGolongan = (TextView) findViewById(R.id.tv_golongan);
        tvTagihan = (TextView) findViewById(R.id.tv_tagihan);
        tvStatus = (TextView) findViewById(R.id.tv_status);


        btn_cekTagihan = findViewById(R.id.btn_cekTagihan);
        btn_cekTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(CekTagihanPDAM.this) {
                    @Override
                    public void onBackPressed() {
                       finish();
                    }
                };

                progressDialog.setMessage("Mohon Tunggu ...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();

                grad_all_data();

            }
            public void grad_all_data(){
                if (checkConn.isConnected(CekTagihanPDAM.this)) {
                    RequestBody token_request = RequestBody.create(MediaType.parse("multipart/form-data"), TOKEN_MOBILE);
                    RequestBody nosal_request = RequestBody.create(MediaType.parse("multipart/form-data"), et_nomorTagihan.getText().toString());

                    base_url_management = Base_url.getClient_https(BASE_URL).create(SendDataTagihan.class);
                    Call<ResponseBody> getdata = base_url_management.get_pdam(token_request, nosal_request);

                    getdata.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            Log.e(TAG, "grad_all_data: ok3");
                            try {

                                Log.e(TAG, "grad_all_data: ok4");
                                String data_response_body = response.body().string();
                                JSONObject data_response = new JSONObject(data_response_body);

                                boolean status = data_response.getBoolean("success");
                                if (status) {
                                    JSONObject data_pelanggan = data_response.getJSONObject("plg");
                                    String nama = data_pelanggan.getString("nama");
                                    String alamat = data_pelanggan.getString("alamat");
                                    String golongan = data_pelanggan.getString("golongan");
                                    String nosal = data_pelanggan.getString("nosal");
                                    String status1 = data_pelanggan.getString("status");

                                    tvNama.setText(nama);
                                    tvAlamat.setText(alamat);
                                    tvNosal.setText(nosal);
                                    tvGolongan.setText(golongan);
                                    tvStatus.setText(status1);

                                    String list_tagihan = data_pelanggan.getString("tagihan");
                                    Log.e(TAG, list_tagihan);

                                    JSONArray array_tagihan = new JSONArray(list_tagihan);
//                                ;

                                Log.e(TAG, String.valueOf(array_tagihan.length()));
                                    for (int i = 0; i < array_tagihan.length(); i++) {
                                        JSONObject periode = new JSONObject(array_tagihan.get(0).toString());
                                        Log.e(TAG, periode.getString("periode"));
                                        Log.e(TAG, periode.getString("total"));

                                        String tagihan = periode.getString("total");
                                        tvTagihan.setText(tagihan);
                                    }
                                    progressDialog.dismiss();
                                    ll_hasilCekTagihan.setVisibility(View.VISIBLE);

                                } else {
                                    progressDialog.dismiss();
                                    ll_hasilCekTagihan.setVisibility(View.INVISIBLE);
                                    showDialog();
                                }

                                Log.e(TAG, data_response_body);

                            } catch (Exception e) {
                                Log.e(TAG, "anu: onResponse: " + e);

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.e(TAG, "add_sp_vendor: onFailure: " + t);
                        }


                    });

                } else {
                    Log.e(TAG, "add_sp_vendor: tidak ada koneksi");
                }


            }
        });

        btn_hapus = findViewById(R.id.btn_hapus);
        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nomorTagihan.setText("");
                tvNama.setText("");
                tvAlamat.setText("");
                tvNosal.setText("");
                tvGolongan.setText("");
                tvStatus.setText("");
                tvTagihan.setText("");
                ll_hasilCekTagihan.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title dialog
        alertDialogBuilder.setTitle("Mohon Maaf !");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Data yang Anda Cari Tidak Tersedia ...")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        return;
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
}
