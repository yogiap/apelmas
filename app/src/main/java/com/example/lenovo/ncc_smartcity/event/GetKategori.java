package com.example.lenovo.ncc_smartcity.event;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by USER on 31/05/2018.
 */

public class GetKategori {
    public String[] spinnerArray;
    public HashMap<Integer, Kategori> spinnerMap;
    public Spinner spinner;

    public void listSemuaKategori(final View v) {
        RequestQueue queue = Volley.newRequestQueue(v.getContext());
        String url = "http://sidia.malangkota.go.id/c_event/eventkategori/";


        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONArray panda = new JSONArray(response);
                            spinnerArray = new String[panda.length()];
                            spinnerMap = new HashMap<Integer, Kategori>();
                            for (int i = 0; i < panda.length(); i++) {
                                JSONObject pandi = panda.getJSONObject(i);
                                spinnerMap.put(i, new Kategori(pandi.getString("id_category"), pandi.getString("category")));
                                spinnerArray[i] = pandi.getString("category");
                            }
                            spinner = (Spinner) v.findViewById(R.id.kategori);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(v.getContext(), android.R.layout.simple_spinner_item, spinnerArray);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner.setAdapter(adapter);
                        } catch (JSONException e) {
                            Log.e("panda", e.toString());
                            Toast.makeText(v.getContext(), "Error, something went wrong!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}

class Kategori {
    public String id_category;
    public String category;

    public Kategori(String id_category, String category) {
        this.id_category = id_category;
        this.category = category;
    }
}
