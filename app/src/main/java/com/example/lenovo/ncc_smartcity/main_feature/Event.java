package com.example.lenovo.ncc_smartcity.main_feature;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.event.CustomAdapterEvent;
import com.example.lenovo.ncc_smartcity.event.DataModel;
import com.example.lenovo.ncc_smartcity.event.EventAdd;
import com.example.lenovo.ncc_smartcity.event.EventProfile;
import com.example.lenovo.ncc_smartcity.info_layanan.Main_Layanan_First_Level;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.ArrayList;

/**
 * Created by lenovo on 2/5/2018.
 */

public class Event extends AppCompatActivity {

    //    ListView listView;
    private static CustomAdapterEvent adapter, adapter2;
    ArrayList<DataModel> dataModels;
    ProgressDialog progressDialog, progressDialog2;
    String loadingText = "Loading";
    private RecyclerView recyclerView, recyclerView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.main_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = URLCollection.DATA_SOURCE_EVENT;
        String url = "http://ncc.malangkota.go.id/smartcity/eventmalang";
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };
        //picolo
        progressDialog.setMessage(loadingText);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
//                        Log.e("apa", response);
                        progressDialog.dismiss();

                        try {
                            dataModels = new ArrayList<>();
                            JSONArray panda = new JSONArray(response);
                            for (int i = 0; i < panda.length(); i++) {
                                JSONObject panda1 = panda.getJSONObject(i);
                                //image dah jadi besar kalau dishare
//                                String nama = "";
//                                for (int j = 0; j < panda1.getJSONArray("category").length(); j++) {
//
//                                    nama += panda1.getJSONArray("category").getJSONObject(j).getString("nama") + ",";
////                                    String link= panda1.getJSONArray("category").getString("nama");
//                                }
//                                nama.substring(0, nama.length() - 1);
                                dataModels.add(new DataModel(panda1.getString("img").replace("-150x150", ""),
                                        panda1.getString("link"),
                                        Jsoup.parse(panda1.getString("title")).text(),
                                        Jsoup.parse(panda1.getString("description")).text(),
                                        "Category : "));

                            }
//                           adapter= new CustomAdapter(dataModels,getApplicationContext());
//
//                            listView.setAdapter(adapter);
                            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

                            adapter = new CustomAdapterEvent(dataModels);

                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Event.this);

                            recyclerView.setLayoutManager(layoutManager);

                            recyclerView.setAdapter(adapter);

                            //mTextView.setText(panda1.getString("img"));
                        } catch (JSONException e) {
                            Log.e("panda", e.toString());
                            Toast.makeText(Event.this, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                        // Getting JSON Array node

//                        mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mTextView.setText("That didn't work!");
                Log.e("apa", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        listSemuaEvent();
    }

    public void listSemuaEvent() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://ncc.malangkota.go.id/smartcity/eventmalang.json";

        progressDialog2 = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };
        //picolo
        progressDialog2.setMessage(loadingText);
        progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog2.setCancelable(false);
        progressDialog2.show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
//                        Log.e("apa", response);
//                    progressDialog2.dismiss();

                        try {
                            dataModels = new ArrayList<>();
                            JSONArray panda = new JSONArray(response);
                            for (int i = 0; i < panda.length(); i++) {
                                JSONObject panda1 = panda.getJSONObject(i);
                                dataModels.add(new DataModel(panda1.getString("gambar"),
                                        "",
                                        panda1.getString("nama"),
                                        panda1.getString("deskripsi"),
                                        panda1.getString("kategori")));
                            }
//                           adapter= new CustomAdapter(dataModels,getApplicationContext());
//
//                            listView.setAdapter(adapter);
                            recyclerView2 = (RecyclerView) findViewById(R.id.recycler_view2);

                            adapter2 = new CustomAdapterEvent(dataModels);

                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Event.this);

                            recyclerView2.setLayoutManager(layoutManager);

                            recyclerView2.setAdapter(adapter2);
                            progressDialog2.dismiss();
                            //mTextView.setText(panda1.getString("img"));
                        } catch (JSONException e) {
                            Log.e("panda", e.toString());
                            Toast.makeText(Event.this, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                        // Getting JSON Array node

//                        mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mTextView.setText("That didn't work!");
                Log.e("apa", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    //hai
    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.event_options, menu);
//        MenuItem item = menu.findItem(R.id.menu_main_search);
//        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.menu_main_search:
                Toast.makeText(this, "search", Toast.LENGTH_SHORT)
                        .show();
                break;
            case R.id.menu_main_add:
                Intent i = new Intent(this, EventAdd.class);
                this.startActivity(i);
                break;
            case R.id.menu_main_profile:
                Intent j = new Intent(this, EventProfile.class);
                this.startActivity(j);
                break;

            default:
                break;
        }

        return true;
    }
}
