package com.example.lenovo.ncc_smartcity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AntrianP extends AppCompatActivity {

    WebView webVA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVA = (WebView)findViewById(R.id.webVP);
        String url = "https://ncctrial.malangkota.go.id/f_kios_v2/beranda/kesehatan/rumah_sakit?layanan=172a2217775c6e0a0e9e4b8cb60266aa2b3689268fc8299c8972a74c0d49ec61a6e03bfd76e6a29be2c967b01d95b2ccfb71377cdde679b8c89fbc023260b82b";
        webVA.getSettings().setJavaScriptEnabled(true);
        webVA.setFocusable(true);
        webVA.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVA.getSettings().setDomStorageEnabled(true);
        webVA.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVA.getSettings().setDatabaseEnabled(true);
        webVA.getSettings().setAppCacheEnabled(true);
        webVA.loadUrl(url);
        webVA.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVA.canGoBack()) {
            webVA.goBack();
        }else {
            super.onBackPressed();
        }
    }
}