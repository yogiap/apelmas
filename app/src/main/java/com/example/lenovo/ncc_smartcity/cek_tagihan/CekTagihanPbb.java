package com.example.lenovo.ncc_smartcity.cek_tagihan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataTagihan;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;

import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.LinearLayout.VERTICAL;

public class CekTagihanPbb extends AppCompatActivity implements View.OnClickListener {
    EditText et_nama, et_nop;
    TextView tv_nop, tv_nama, tv_alamat;
    Button btn_cek,btn_hapus;
    ProgressDialog progressDialog;
    LinearLayout ll_hasilCekTagihan;

    RecyclerView recyclerView;

    ArrayList<Model_tagihan> model_tagihan;
    private Adapter_tagihan adapter_tagihan;

    SendDataTagihan other_requets;

    String url_sambat = URLCollection.BASE_URL_NCC;

    String TAG = "CekTagihanPbb";

    public Context get_activity(){
        return CekTagihanPbb.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_pbb);

        et_nop = (EditText) findViewById(R.id.et_nomorpbb);
        et_nama = (EditText) findViewById(R.id.et_namapbb);

        btn_cek = (Button) findViewById(R.id.btn_cekTagihan);
        btn_hapus = (Button) findViewById(R.id.btn_hapus);

        tv_nama     = (TextView) findViewById(R.id.tv_nama);
        tv_alamat   = (TextView) findViewById(R.id.tv_alamat);
        tv_nop      = (TextView) findViewById(R.id.tv_nop);

        recyclerView = (RecyclerView) findViewById(R.id.rv_tagihan);
        ll_hasilCekTagihan = findViewById(R.id.ll_cekTagihanHasil);


        btn_cek.setOnClickListener(this);

        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nop.setText("");
                et_nama.setText("");
                tv_nop.setText("");
                tv_nama.setText("");
                tv_alamat.setText("");
                ll_hasilCekTagihan.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        progressDialog = new ProgressDialog(CekTagihanPbb.this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage("Mohon Tunggu ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        switch (v.getId()){
            case R.id.btn_cekTagihan:
                Log.e(TAG, "btn_cek");
                grad_all_data();
                break;
        }
    }

    public void grad_all_data(){
        Log.e(TAG, "grad_all_data: ok1");

        String param_nop = et_nop.getText().toString();
        String param_nama = et_nama.getText().toString();

        if(new CheckConn().isConnected(get_activity())){
            Log.e(TAG, "grad_all_data: ok2");
            RequestBody nop     = RequestBody.create(MediaType.parse("multipart/form-data"), param_nop);
            RequestBody nama_wp = RequestBody.create(MediaType.parse("multipart/form-data"), param_nama);


            other_requets = Base_url.getClient_https(url_sambat).create(SendDataTagihan.class);
            Call<ResponseBody> getdata = other_requets.get_pbb(nop,nama_wp);

            getdata.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    Log.e(TAG, "grad_all_data: ok3");
                    try {

                        Log.e(TAG, "grad_all_data: ok4");
                        String data_response_body = response.body().string();
                        JSONObject data_response = new JSONObject(data_response_body);
                        Log.e(TAG, data_response_body);

                        JSONObject msg_main = new JSONObject(data_response.getString("msg_main"));
                        JSONObject msg_detail = new JSONObject(data_response.getString("msg_detail"));

                        String msg = msg_main.getString("msg");

                        boolean status = msg_main.getBoolean("status");
//                        Log.e(TAG, msg);
                        if(status){
                            JSONObject data_pelanggan = new JSONObject(msg_detail.getString("list_result"));
                            String nop = data_pelanggan.getString("nop");
                            String nama_wp = data_pelanggan.getString("nama_wp");
                            String alamat_op = data_pelanggan.getString("alamat_op");
//                            String nosal = data_pelanggan.getString("nosal");

                            tv_nop.setText(nop);
                            tv_nama.setText(nama_wp);
                            tv_alamat.setText(alamat_op);
//                            tv_nosal.setText(nosal);

                            String list_tagihan = data_pelanggan.getString("tahun_tunggakan");
                            Log.e(TAG, list_tagihan);

                            JSONArray array_tagihan = new JSONArray(list_tagihan);

                            model_tagihan = new ArrayList<>();
                            for (int i = 0; i<array_tagihan.length(); i++){
                                final JSONObject json_tagihan = new JSONObject(array_tagihan.get(i).toString());
                                Iterator keys = json_tagihan.keys();
                                while(keys.hasNext() ) {
                                    String key = (String)keys.next();
                                    String nominal = json_tagihan.getString(key);
                                    Log.e(TAG, "key : "+key+", nominal : "+nominal);

                                    model_tagihan.add(new Model_tagihan(
                                            key,
                                            nominal)
                                    );
                                }
                            }
                            progressDialog.dismiss();
                            ll_hasilCekTagihan.setVisibility(View.VISIBLE);
                            set_recycle();

                        }else {
                            Log.e(TAG, "false");

                        }

//                        JSONObject main_data = new JSONObject(data_response.getJSONObject("msg_main").toString());
//                        JSONObject detail_data = new JSONObject(data_response.getJSONObject("msg_detail").toString());



                    } catch (Exception e) {
                        Log.e(TAG, "add_sp_vendor: onResponse: " + e);
                        et_nop.setText("");
                        et_nama.setText("");
                        tv_nop.setText("");
                        tv_nama.setText("");
                        tv_alamat.setText("");
                        ll_hasilCekTagihan.setVisibility(View.INVISIBLE);
                        progressDialog.dismiss();
                        showDialog();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "add_sp_vendor: onFailure: " + t);
                }


            });


        }else {
            Log.e(TAG, "add_sp_vendor: tidak ada koneksi");
        }


    }

    public void set_recycle(){
        recyclerView = (RecyclerView) findViewById(R.id.rv_tagihan);
        adapter_tagihan = new Adapter_tagihan(model_tagihan);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(get_activity() , VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter_tagihan);
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title dialog
        alertDialogBuilder.setTitle("Mohon Maaf !");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Data yang Anda Cari Tidak Tersedia ...")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        return;
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
}