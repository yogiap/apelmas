package com.example.lenovo.ncc_smartcity.pendidikan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.lenovo.ncc_smartcity.R;

public class Perpustakaan extends AppCompatActivity {

    WebView webVPe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVPe = (WebView)findViewById(R.id.webVP);
        String url = "https://ncctrial.malangkota.go.id/f_kios_v2/beranda/pendidikan/41f4bc8c99de770a5695285fd42e42e6e6f0664396eafddb2524f29febcd8b8d?menu=dad3fafe0e1232e4079f5e62cff0b10e6b0c3958f184d6009ef2ff6f547bdf6d532cc867d66e17d4e4797a97c72115f6dc2fcc2da9210778f558ff04e540f173";
        webVPe.getSettings().setJavaScriptEnabled(true);
        webVPe.setFocusable(true);
        webVPe.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVPe.getSettings().setDomStorageEnabled(true);
        webVPe.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVPe.getSettings().setDatabaseEnabled(true);
        webVPe.getSettings().setAppCacheEnabled(true);
        webVPe.loadUrl(url);
        webVPe.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVPe.canGoBack()) {
            webVPe.goBack();
        }else {
            super.onBackPressed();
        }
    }
}

