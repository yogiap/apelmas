package com.example.lenovo.ncc_smartcity.transportasi;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.R;

public class Stasiun extends AppCompatActivity {

    ImageView img_baru, img_lama, img_blimbing;
    String goolgeMap = "com.google.android.apps.maps"; // identitas package aplikasi google masps android
    Uri gmmIntentUri;
    Intent mapIntent;
    String stasiunbaru = "-7.9774938,112.6348673";
    String stasiunlama = "-7.9950072,112.6331219";
    String stasiunblimbing = "-7.9404262,112.644683";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stasiun);

        img_baru = (ImageView) findViewById(R.id.img_baru);
        img_lama = (ImageView) findViewById(R.id.img_lama);
        img_blimbing = (ImageView) findViewById(R.id.img_blimbing);

        img_baru.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                gmmIntentUri = Uri.parse("google.navigation:q=" + stasiunbaru);

                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

                mapIntent.setPackage(goolgeMap);

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(Stasiun.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }

            }
        });

        img_lama.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                gmmIntentUri = Uri.parse("google.navigation:q=" + stasiunlama);

                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

                mapIntent.setPackage(goolgeMap);

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(Stasiun.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }

            }
        });

        img_blimbing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                gmmIntentUri = Uri.parse("google.navigation:q=" + stasiunblimbing);

                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

                mapIntent.setPackage(goolgeMap);

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(Stasiun.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
