package com.example.lenovo.ncc_smartcity.pasar.fragment;


import android.app.ProgressDialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.event.CustomAdapterEvent;
import com.example.lenovo.ncc_smartcity.event.DataModel;
import com.example.lenovo.ncc_smartcity.pasar.Adapter_pasar;
import com.example.lenovo.ncc_smartcity.pasar.Model_Pasar;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class pasar_fragment_sembako extends Fragment {
    ArrayList<DataModel> dataModels;
    String DATA_SOURCE_URL = URLCollection.DATA_SOURCE_PASAR;
    RecyclerView recyclerView;
    Adapter_pasar adapter_pasar;
    List<Model_Pasar> list_pasar;
    int searchSelesai = 0;
    ProgressDialog progressDialog;
    TextView kosong;
    String loadingMessage = "Loading";
    LinearLayout ll_sumber_pasar;
    MaterialSearchView searchView;

    public pasar_fragment_sembako() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pasar_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ll_sumber_pasar = view.findViewById(R.id.ll_sumber_pasar);
        recyclerView = view.findViewById(R.id.pasar_recyclerView);
        recyclerView.hasFixedSize();
        list_pasar = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
//listSemuaEvent(view);
        initializeDataPasar(view);
//        Date c = Calendar.getInstance().getTime();
//        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//        String formattedDate = df.format(c);

    }

    private void initializeDataPasar(final View view) {
        progressDialog = new ProgressDialog(view.getContext()) {
            @Override
            public void onBackPressed() {
                getActivity().finish();
            }
        };
        //picolo
        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = "http://sidia.malangkota.go.id/smartcity/smart/hargapasar/sembako";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonObject = new JSONArray(response);
                            Log.e("sembako", response);
                            list_pasar.clear();
//                            Log.e("pasar",Integer.toString(jsonObject.getJSONArray("sembako").length()));
                            for (int i = 0; i < jsonObject.length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject(i);

                                Model_Pasar modelPasar = new Model_Pasar(
                                        jsonObject1.getString("nama"),
                                        jsonObject1.getString("jenis"),
                                        jsonObject1.getString("satuan"),
                                        jsonObject1.getString("harga_kemarin"),
                                        jsonObject1.getString("tanggal")
                                );
                                list_pasar.add(modelPasar);
                            }

                            adapter_pasar = new Adapter_pasar(list_pasar, view.getContext());
                            recyclerView.setAdapter(adapter_pasar);
                            ll_sumber_pasar.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(view.getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("VolleyError", error.toString());
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(view.getContext());
        requestQueue.add(stringRequest);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


}
