package com.example.lenovo.ncc_smartcity.fragmen;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.AboutUs;
import com.example.lenovo.ncc_smartcity.MainAntrian;
import com.example.lenovo.ncc_smartcity.MainEkonomi;
import com.example.lenovo.ncc_smartcity.MainTransportasi;
import com.example.lenovo.ncc_smartcity.MenuBaru;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity.Sambat;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.berita.Item;
import com.example.lenovo.ncc_smartcity.berita.Item1;
import com.example.lenovo.ncc_smartcity.berita.ItemAdapter;
import com.example.lenovo.ncc_smartcity.berita.ItemAdapterAtas;
import com.example.lenovo.ncc_smartcity.berita.MainBerita;
import com.example.lenovo.ncc_smartcity.berita.MainTemplate;
import com.example.lenovo.ncc_smartcity.cctv.Cctv;
import com.example.lenovo.ncc_smartcity.cek_tagihan.CekTagihan;
import com.example.lenovo.ncc_smartcity.event.Event_New_Layout;
import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.MainIdrekan;
import com.example.lenovo.ncc_smartcity.main_feature.Darurat;
import com.example.lenovo.ncc_smartcity.main_feature.Layanan;
import com.example.lenovo.ncc_smartcity.main_feature.Pariwisata;
import com.example.lenovo.ncc_smartcity.main_feature.Pasar_New_Layout;
import com.example.lenovo.ncc_smartcity.pariwisata.InfoWisata;
import com.example.lenovo.ncc_smartcity.tanggap.Main_tanggap;
import com.example.lenovo.ncc_smartcity.tutorial.Tutorial;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import butterknife.OnClick;


import static com.google.android.gms.internal.zzahg.runOnUiThread;

public class HomeFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    int page = 1;
    String title, base_url, base_url1, hostname_adapter, spec;
    ProgressDialog progressDialog;
    String loadingText = "Loading";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView, recyclerView1;
    private ItemAdapter adapter_item;
    private ItemAdapterAtas adapter_item1;
    private ArrayList<Item> itemArrayList = new ArrayList<>();
    private ArrayList<Item1> item1ArrayList = new ArrayList<>();
    private RoundedImageView cc, sambat, antrian, layanan, call, cctv, etravel, lowongan, ekonomi, transport, event, tutorial, tentang,
            tagihan;

    ArrayList dataJadwalSholat;
    ArrayList dataCuaca;
    String date, subuh, dzuhur, ashar, maghrib, isya, sumber;
    String NO_DARURAT = "112";

    private RequestQueue mQueue;

    final int duration = 10;
    final int pixelsToMove = 30;
    private final Handler mHandler = new Handler(Looper.getMainLooper());


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        Intent intent = getActivity().getIntent();

        tagihan = (RoundedImageView) view.findViewById(R.id.p_tagihan);
        tagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister = new Intent(getActivity(), CekTagihan.class);
                startActivity(iRegister);
            }
        });
        layanan = (RoundedImageView) view.findViewById(R.id.riv_layanan);
        layanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister = new Intent(getActivity(), Layanan.class);
                startActivity(iRegister);
            }
        });
        call = (RoundedImageView) view.findViewById(R.id.riv_call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister = new Intent(getActivity(), Darurat.class);
                startActivity(iRegister);
            }
        });
        cctv = (RoundedImageView) view.findViewById(R.id.riv_cctv);
        cctv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Cctv.class);
                startActivity(i);
                //showDialog();
            }
        });
        etravel = (RoundedImageView) view.findViewById(R.id.riv_etravel);
        etravel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Pariwisata.class);
                startActivity(i);
            }
        });
        lowongan = (RoundedImageView) view.findViewById(R.id.riv_lowongan);
        lowongan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainIdrekan.class);
                startActivity(i);
            }
        });
        ekonomi = (RoundedImageView) view.findViewById(R.id.riv_ekonomi);
        ekonomi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainEkonomi.class);
                startActivity(i);
            }
        });
        transport = (RoundedImageView) view.findViewById(R.id.riv_transport);
        transport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainTransportasi.class);
                startActivity(i);
            }
        });
        event = (RoundedImageView) view.findViewById(R.id.riv_event);
        event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Event_New_Layout.class);
                startActivity(i);
            }
        });
//        tutorial = (RoundedImageView) view.findViewById(R.id.riv_tutorial);
//        tutorial.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getActivity(), Tutorial.class);
//                startActivity(i);
//            }
//        });
//        tentang = (RoundedImageView) view.findViewById(R.id.riv_tentang);
//        tentang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getActivity(), AboutUs.class);
//                startActivity(i);
//            }
//        });

        cc = (RoundedImageView) view.findViewById(R.id.p_cc);
        cc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calls = new Intent(Intent.ACTION_DIAL);
                calls.setData(Uri.parse("tel:" + NO_DARURAT));
                startActivity(calls);
            }
        });

        antrian = (RoundedImageView) view.findViewById(R.id.p_antrian);
        antrian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainAntrian.class);
                startActivity(i);
            }
        });

        sambat = (RoundedImageView) view.findViewById(R.id.p_sambat);
        sambat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Sambat.class);
                startActivity(i);
            }
        });


        title = intent.getStringExtra("title");
        base_url = "https://mediacenter.malangkota.go.id/category/berita/features/";
        base_url1 = "https://mediacenter.malangkota.go.id/category/utama/";

        TextView tv =  view.findViewById(R.id.tv_rollingTextJadwalSholat);
        tv.setSelected(true);
        ButterKnife.bind(this,view);
        getJadwalSholat();
        getCuacaOpenWeather();

        getWebsite();
        getWebsite2();

        return view;
    }


    @Override
    public void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

    private void getWebsite() {

        progressDialog = new ProgressDialog(getActivity()) {
            @Override
            public void onBackPressed() {
                getActivity().finish();
            }
        };

        //picolo
        progressDialog.setMessage(loadingText);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        new Thread(new Runnable() {
            public void run() {
                String page_str;
                String x;
                if (page == 0) {
                    page_str = "";
                } else if (page > 0) {
                    page_str = "page/" + String.valueOf(page) + "/";
                } else {
                    page_str = "";
                }
                final StringBuilder builder = new StringBuilder();

                HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        HostnameVerifier hv =
                                HttpsURLConnection.getDefaultHostnameVerifier();
                        return hv.verify(hostname_adapter, session);
                    }
                };

                // Tell the URLConnection to use our HostnameVerifier
                try {
                    if (!Thread.interrupted()) {
                        URL url = new URL(spec);
                        HttpsURLConnection urlConnection =
                                (HttpsURLConnection) url.openConnection();
                        urlConnection.setHostnameVerifier(hostnameVerifier);
                        InputStream in = urlConnection.getInputStream();
                    }
                } catch (Exception e) {
                    Log.e("exception", "run: " + e);
                }
                try {
                    System.setProperty("jsse.enableSNIExtension", "false");
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCertificates, new SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames);
                }
                catch (GeneralSecurityException e) {
                    throw new ExceptionInInitializerError(e);
                }
                try {
                    //System.setProperty("javax.net.ssl.trustStore","http://embohgakeroh.000webhostapp.com/-_malangkota_go_id.jks");
                    Document doc = Jsoup.connect(base_url + page_str)
                            .validateTLSCertificates(false)
                            .data("first", "true")
                            .ignoreContentType(true)
                            .userAgent("Mozilla")
                            .cookie("auth", "token")
                            .timeout(10000)
                            .post();

                    Elements links = doc.select("article");

                    for (Element link : links) {
                        Elements gmbar = link.select("img");
                        Elements desc_last = link.select("p");
                        String link_text = link.select("h2").
                                select("a[href]").
                                attr("href");
                        String title_text =
                                link.select("h2").select("a").text();
                        String img_text =
                                gmbar.first().select("img[src]").attr("src");
                        String description = desc_last.last().
                                select("p").text();
                        try {
                            itemArrayList.add(new Item(title_text, img_text, description, "SELENGKAPNYA", link_text));
                        } catch (Exception e) {
                            Log.e("error_data", "run: " + e);
                        }
                    }
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                    Log.e("error_get", "run: " + e);
                } catch (Exception ex) {
                    Log.e("error_get", "run: " + ex);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        recyclerView = getView().findViewById(R.id.recycler_view);
                        adapter_item = new ItemAdapter(itemArrayList);
                        Log.e("adapter_item", "run: " + itemArrayList);

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter_item);
                        recyclerView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("view", "onClick: " + v);

                            }
                        });

                    }
                });
                progressDialog.dismiss();
            }
        }).start();
    }

    private void getWebsite2() {
        new Thread(new Runnable() {
            public void run() {
                String page_str;
                if (page == 0) {
                    page_str = "";
                } else if (page > 0) {
                    page_str = "page/" + String.valueOf(page) + "/";
                } else {
                    page_str = "";
                }
                final StringBuilder builder = new StringBuilder();

                HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        HostnameVerifier hv =
                                HttpsURLConnection.getDefaultHostnameVerifier();
                        return hv.verify(hostname_adapter, session);
                    }
                };

                // Tell the URLConnection to use our HostnameVerifier
                try {
                    if (!Thread.interrupted()) {
                        URL url = new URL(spec);
                        HttpsURLConnection urlConnection =
                                (HttpsURLConnection) url.openConnection();
                        urlConnection.setHostnameVerifier(hostnameVerifier);
                        InputStream in = urlConnection.getInputStream();
                    }
                } catch (Exception e) {
                    Log.e("exception", "run: " + e);
                }
                try {
                    Document doc = Jsoup.connect(base_url1 + page_str).data("query", "Java")
                            .userAgent("Mozilla")
                            .cookie("auth", "token")
                            .timeout(10000)
                            .post();

                    Elements links1 = doc.select("article");

                    for (Element link1 : links1) {
                        Elements gmbar1 = link1.select("img");
                        Elements desc_last1 = link1.select("p");
                        String link_text1 = link1.select("h2").
                                select("a[href]").
                                attr("href");
                        String title_text1 =
                                link1.select("h2").select("a").text();
                        String img_text1 =
                                gmbar1.first().select("img[src]").attr("src");
                        String description1 = desc_last1.last().
                                select("p").text();
                        try {
                            item1ArrayList.add(new Item1(title_text1, img_text1, link_text1));
                        } catch (Exception e) {
                            Log.e("error_data", "run: " + e);
                        }
                    }
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                    Log.e("error_get", "run: " + e);
                } catch (Exception ex) {
                    Log.e("error_get", "run: " + ex);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        recyclerView1 = getView().findViewById(R.id.recycler_view1);
                        adapter_item1 = new ItemAdapterAtas(item1ArrayList);
                        Log.e("adapter_item", "run: " + item1ArrayList);
                        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                        PagerSnapHelper snapHelper = new PagerSnapHelper();
                        snapHelper.attachToRecyclerView(recyclerView1);
                        recyclerView1.addItemDecoration(new LinePagerIndicatorDecoration());
                        recyclerView1.setLayoutManager(layoutManager1);
                        recyclerView1.setAdapter(adapter_item1);
                        recyclerView1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("view", "onClick: " + v);

                            }
                        });

                    }
                });
                progressDialog.dismiss();
            }
        }).start();
    }

    private void getCuacaOpenWeather() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = URLCollection.DATA_SOURCE_OPEN_WEATHER; //copy url disini APInya

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dataCuaca = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("weather");
                    JSONObject jsonObject_result = jsonArray.getJSONObject(0);

                    String getWeather = jsonObject_result.getString("description");
                    String getIcon = "ic_weather_" + jsonObject_result.getString("icon");
                    Log.d("GetCuacaOW", "getIcon : " + getIcon);

                    String getTemperature = jsonObject.getJSONObject("main").getString("temp") + "C";

                    TextView tv_weather = getActivity().findViewById(R.id.tv_cuaca);
                    TextView tv_temperature = getActivity().findViewById(R.id.tv_cuaca_temperature_current);
                    ImageView iv_weatherIcon = getActivity().findViewById(R.id.iv_weatherIcon);

                    Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "font/myriad_pro_regular.otf");

                    tv_weather.setTypeface(face);
                    tv_temperature.setTypeface(face);


                    int iconID = getResources().getIdentifier(getIcon, "drawable", getActivity().getPackageName());
                    Log.d("GetCuacaOW", "iconID : " + iconID);

                    iv_weatherIcon.setImageResource(iconID);

                    tv_weather.setText(getWeather);
                    tv_temperature.setText(getTemperature);

                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Error, something went wrong!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @OnClick({R.id.tv_rollingTextJadwalSholat})
    public void jadwalSholatPopUp() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.jadwal_sholat_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tanggalText = (TextView) dialogView.findViewById(R.id.tanggal);
        tanggalText.setText(date);
        TextView subuhText = (TextView) dialogView.findViewById(R.id.subuh);
        subuhText.setText(subuh);
        TextView dzuhurText = (TextView) dialogView.findViewById(R.id.dzuhur);
        dzuhurText.setText(dzuhur);
        TextView asharText = (TextView) dialogView.findViewById(R.id.ashar);
        asharText.setText(ashar);
        TextView maghribText = (TextView) dialogView.findViewById(R.id.maghrib);
        maghribText.setText(maghrib);
        TextView isyaText = (TextView) dialogView.findViewById(R.id.isya);
        isyaText.setText(isya);
        TextView sumberText = (TextView) dialogView.findViewById(R.id.sumber);
        sumberText.setText(sumber);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    private void getJadwalSholat() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = URLCollection.DATA_SOURCE_JADWAL_SHOLAT_PKPU; //copy url disini APInya
        final TextView tv_rollingText = getActivity().findViewById(R.id.tv_rollingTextJadwalSholat);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "font/myriad_pro_regular.otf");

        try {
            tv_rollingText.setTypeface(face);
            Log.e("su", "onResponse: run");
        } catch (Exception e) {
            Log.e("su", "onResponse: " + e.getMessage());
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    dataJadwalSholat = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    date = "Jadwal Waktu Sholat Untuk Tanggal " + jsonObject.getString("tanggal") + " ";

                    subuh = "Subuh : " + jsonObject.getString("shubuh") + "  ";
                    dzuhur = "Dzuhur : " + jsonObject.getString("dzuhur") + "  ";
                    ashar = "Ashar : " + jsonObject.getString("ashar") + "  ";
                    maghrib = "Maghrib : " + jsonObject.getString("maghrib") + "  ";
                    isya = "Isya :" + jsonObject.getString("isya") + "  ";

                    sumber = "Sumber : " + jsonObject.getString("sumber");

                    String rollingText = date + " " + subuh + dzuhur + ashar + maghrib + isya;
                    final TextView tv_rollingText = getActivity().findViewById(R.id.tv_rollingTextJadwalSholat);
                    tv_rollingText.setText(rollingText);
                    date = jsonObject.getString("tanggal");

                    subuh = jsonObject.getString("shubuh");
                    dzuhur = jsonObject.getString("dzuhur");
                    ashar = jsonObject.getString("ashar");
                    maghrib = jsonObject.getString("maghrib");
                    isya = jsonObject.getString("isya");

                    sumber = jsonObject.getString("sumber");

                } catch (JSONException e) {
                    Log.e("errorjson", e.toString());
                    Toast.makeText(getActivity(), "Error, something went wrong!" + e.toString(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set title dialog
        alertDialogBuilder.setTitle("Mohon Maaf!");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Saat ini Fitur CCTV Belum Tersedia..")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        getActivity();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    TrustManager[] trustAllCertificates = new TrustManager[] {
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null; // Not relevant.
                }
                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    // Do nothing. Just allow them all.
                }
                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    // Do nothing. Just allow them all.
                }
            }
    };

    HostnameVerifier trustAllHostnames = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true; // Just allow them all.
        }
    };



}
