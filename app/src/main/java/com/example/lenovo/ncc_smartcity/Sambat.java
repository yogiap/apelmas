package com.example.lenovo.ncc_smartcity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Sambat extends AppCompatActivity {

    WebView webV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sambat);

        webV = (WebView)findViewById(R.id.webV);
        String url = "https://sambat.malangkota.go.id";
        webV.getSettings().setJavaScriptEnabled(true);
        webV.setFocusable(true);
        webV.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webV.getSettings().setDomStorageEnabled(true);
        webV.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webV.getSettings().setDatabaseEnabled(true);
        webV.getSettings().setAppCacheEnabled(true);
        webV.loadUrl(url);
        webV.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webV.canGoBack()) {
            webV.goBack();
        }else {
            super.onBackPressed();
        }
    }
}