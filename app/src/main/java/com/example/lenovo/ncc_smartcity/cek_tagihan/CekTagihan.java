package com.example.lenovo.ncc_smartcity.cek_tagihan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.example.lenovo.ncc_smartcity.R;

public class CekTagihan extends Activity {

    CardView ct_pdam, ct_pbb;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_tagihan_home);

        ct_pdam = findViewById(R.id.ct_pdam);
        ct_pdam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(CekTagihan.this, CekTagihanPDAM.class);
                startActivity(intent);
            }
        });
        ct_pbb = findViewById(R.id.ct_pbb);
        ct_pbb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(CekTagihan.this, CekTagihanPbb.class);
                startActivity(intent);
            }
        });
    }
}
