package com.example.lenovo.ncc_smartcity.berita;

public class Item1 {
    private String title1;
    private String image1;
    private String link1;


    public Item1(String title1, String image1, String link1) {
        this.title1 = title1;
        this.image1 = image1;
        this.link1 = link1;
    }

    public String getLink1() {
        return link1;
    }

    public void setLink1(String link1) {
        this.link1 = link1;
    }


    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title11) {
        this.title1 = title1;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

}
