package com.example.lenovo.ncc_smartcity.dashAkun;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.MainActivity;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.example.lenovo.ncc_smartcity.internalLib.Session;
import com.example.lenovo.ncc_smartcity.internalLib.TextSafety;
import com.example.lenovo.ncc_smartcity.login.MainLogin;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 13/04/2018.
 */

public class ChPass extends FragmentActivity {
    Button btnSave, btnCancle;
    EditText etPassBaru, etRePassBaru, etPassOld;

    TextSafety textSafety = new TextSafety();
    CheckConn checkConn = new CheckConn();

    String TAG = "ChPass",
            message_fail_con = "untuk mengganti password mohon aktifkan jaringan anda",
            message_fail_request = "request gagal mohon coba beberapa saat lagi",
            message_fail_input = "mohon perikas lagi inputan anda dengan benar",
            message_fail_repass = "input password dan ulangi password tidak sama, mohon perikasa lagi",
            message_loaading = "mohon tunggu proses sedang berjalan";


    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";

    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;

    ProgressDialog progressDialog;
    Intent intent;

    String user_id = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dash_ch_pass);

        btnSave = (Button) findViewById(R.id.btnSave);
//        btnCancle = (Button) findViewById(R.id.btnCan);

        etPassOld = (EditText) findViewById(R.id.etPassOld);
        etPassBaru = (EditText) findViewById(R.id.etPassBaru);
        etRePassBaru = (EditText) findViewById(R.id.etRePassBaru);

        intent = getIntent();
        user_id = intent.getStringExtra("user_id");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckInput()) {
                    if (etPassBaru.getText().toString().equals(etRePassBaru.getText().toString())) {
                        sendData();
                    } else {
                        Toast.makeText(ChPass.this, message_fail_repass, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(ChPass.this, message_fail_con, Toast.LENGTH_LONG).show();
                }
            }
        });

//        btnCancle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

    }


    private boolean CheckInput() {
        if (textSafety.get_valid_char(etPassOld.getText().toString())
                && textSafety.get_valid_char(etPassBaru.getText().toString())
                && textSafety.get_valid_char(etRePassBaru.getText().toString())
        ) {

            return true;
        }
        return false;
    }

    private void sendData() {
        progressDialog = new ProgressDialog(ChPass.this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(message_loaading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        base_url_management = Base_url.getClient_notGson(BASE_URL)
                .create(SendDataLogin.class);
        getdata = base_url_management.UpdatePass(user_id
                , etPassBaru.getText().toString()
                , etPassOld.getText().toString()
                , TOKEN_MOBILE);

        setData(getdata);
    }

    public void setData(Call<ResponseBody> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                String message_out = null;
                try {
                    try {
                        dataJson = new JSONObject(response.body().string());
                        Log.e(TAG, "onResponse: " + dataJson);
                        String status = dataJson.getJSONObject("body_message").getString("status");
                        message_out = dataJson.getJSONObject("body_message").getString("message");
                        if (Boolean.valueOf(status)) {
                            finish();
                        }
                        Toast.makeText(ChPass.this, message_out, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Log.e(TAG, "onResponse: " + e.getMessage());
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: dataJson: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ChPass.this, message_fail_request, Toast.LENGTH_LONG).show();
            }
        });

    }
}
