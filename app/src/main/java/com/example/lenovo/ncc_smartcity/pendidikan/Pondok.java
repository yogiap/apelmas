package com.example.lenovo.ncc_smartcity.pendidikan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.lenovo.ncc_smartcity.R;

public class Pondok extends AppCompatActivity {

    WebView webVPe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVPe = (WebView)findViewById(R.id.webVP);
        String url = "ncctrial.malangkota.go.id/f_kios_v2/beranda/pendidikan/fc0bbb11535d92ec0dcf43b83967c88b24c4bdc28dd7343c3478d8c99cb8a3bd?menu=6787b983db91fe2c605229ea0cf097d8b4dc0e1d526bdfe784965bf7535b9799c8096cf5282dd530cd6cd648a22e12d473cba7e32d9878ffd866ed8c46b141e6";
        webVPe.getSettings().setJavaScriptEnabled(true);
        webVPe.setFocusable(true);
        webVPe.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVPe.getSettings().setDomStorageEnabled(true);
        webVPe.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVPe.getSettings().setDatabaseEnabled(true);
        webVPe.getSettings().setAppCacheEnabled(true);
        webVPe.loadUrl(url);
        webVPe.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVPe.canGoBack()) {
            webVPe.goBack();
        }else {
            super.onBackPressed();
        }
    }
}

