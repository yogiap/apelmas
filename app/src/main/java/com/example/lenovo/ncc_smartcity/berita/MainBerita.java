package com.example.lenovo.ncc_smartcity.berita;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.CardView;

import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity.fragmen.HomeFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by USER on 22/02/2018.
 */

public class MainBerita extends Activity {

    String hostname = "www.malangkota.go.id";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_berita);
        ButterKnife.bind(this);
    }

    private void categoryChooser(String code, String title, String baseURL) {
        intent = new Intent(MainBerita.this, HomeFragment.class);
        intent.putExtra("code", code);
        intent.putExtra("hostname", URLCollection.DATA_SOURCE_BERITA_HOST);
        intent.putExtra("title", title);
        intent.putExtra("base_url", baseURL);
        startActivity(intent);
    }

    @OnClick({R.id.home, R.id.efeature, R.id.ekonomi, R.id.hukum, R.id.layanan, R.id.olahraga,
            R.id.pembangunan, R.id.pendidikan, R.id.sosial, R.id.hiburan})
    void OnCategorySelect(CardView category) {
        switch (category.getId()) {
            case R.id.home:
                categoryChooser("1", "Home", URLCollection.DATA_SOURCE_BERITA_UTAMA);
                break;
            case R.id.efeature:
                categoryChooser("2", "Feature", URLCollection.DATA_SOURCE_BERITA_FEATURES);
                break;
            case R.id.ekonomi:
                categoryChooser("3", "Ekonomi dan Bisnis", URLCollection.DATA_SOURCE_BERITA_EKONOMI_BISNIS);
                break;
            case R.id.hukum:
                categoryChooser("4", "Hukum, Politik dan Pemerintahan", URLCollection.DATA_SOURCE_BERITA_POLITIK);
                break;
            case R.id.layanan:
                categoryChooser("5", "Layanan Publik", URLCollection.DATA_SOURCE_BERITA_LAYANAN_PUBLIK);
                break;
            case R.id.olahraga:
                categoryChooser("6", "Olahraga", URLCollection.DATA_SOURCE_BERITA_OLAHRAGA);
                break;
            case R.id.pembangunan:
                categoryChooser("7", "Pembangunan dan Lingkungan Hidup", URLCollection.DATA_SOURCE_BERITA_PEMBANGUNAN_LINGKUNGAN);
                break;
            case R.id.pendidikan:
                categoryChooser("8", "Pendidikan", URLCollection.DATA_SOURCE_BERITA_PENDIDIKAN);
                break;
            case R.id.sosial:
                categoryChooser("9", "Sosial dan Kesra", URLCollection.DATA_SOURCE_BERITA_KESRA);
                break;
            case R.id.hiburan:
                categoryChooser("10", "Hiburan", URLCollection.DATA_SOURCE_BERITA_HIBURAN);
                break;
        }
    }
}
