package com.example.lenovo.ncc_smartcity.berita;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import com.example.lenovo.ncc_smartcity.R;

import butterknife.OnClick;

/**
 * Created by USER on 22/02/2018.
 */

public class MainTemplate extends AppCompatActivity {

    int page = 1;

    String title, base_url, hostname_adapter, spec;

    LinearLayout linearLayoutSumber;

    ProgressDialog progressDialog;
    String loadingText = "Loading";
    Button btn_next, btn_prev;
    private RecyclerView recyclerView;
    private ItemAdapter adapter_item;
    private ArrayList<Item> itemArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.berita_main_recycler);

        linearLayoutSumber = findViewById(R.id.ll_sumber_berita);
        btn_next = findViewById(R.id.btn_next);
        btn_prev = findViewById(R.id.btn_prev);
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        base_url = intent.getStringExtra("base_url");
        hostname_adapter = intent.getStringExtra("hostname");
        spec = "https://" + hostname_adapter + "/";

        Log.e("hostname_adapter", "hostname_adapter: " + hostname_adapter);
        Log.e("hostname_adapter", "spec: " + hostname_adapter);

        getWebsite();

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page >= 1) {
                    page += 1;
                }
                itemArrayList = new ArrayList<>();
                getWebsite();
            }
        });

        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page >= 1) {
                    page -= 1;
                }
                itemArrayList = new ArrayList<>();
                getWebsite();
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

    private void getWebsite() {

        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        //picolo
        progressDialog.setMessage(loadingText);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        new Thread(new Runnable() {
            public void run() {
                String page_str;
                if (page == 0) {
                    page_str = "";
                } else if (page > 0) {
                    page_str = "page/" + String.valueOf(page) + "/";
                } else {
                    page_str = "";
                }
                final StringBuilder builder = new StringBuilder();

                HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        HostnameVerifier hv =
                                HttpsURLConnection.getDefaultHostnameVerifier();
                        return hv.verify(hostname_adapter, session);
                    }
                };

                // Tell the URLConnection to use our HostnameVerifier
                try {
                    if (!Thread.interrupted()) {
                        URL url = new URL(spec);
                        HttpsURLConnection urlConnection =
                                (HttpsURLConnection) url.openConnection();
                        urlConnection.setHostnameVerifier(hostnameVerifier);
                        InputStream in = urlConnection.getInputStream();
                    }
                } catch (Exception e) {
                    Log.e("exception", "run: " + e);
                }
                try {
                    Document doc = Jsoup.connect(base_url + page_str).data("query", "Java")
                            .userAgent("Mozilla")
                            .cookie("auth", "token")
                            .timeout(10000)
                            .post();

                    Elements links = doc.select("article");

                    for (Element link : links) {

                        Elements gmbar = link.select("img");

                        Elements desc_last = link.select("p");

                        String link_text = link.select("h2").
                                select("a[href]").
                                attr("href");
                        String title_text =
                                link.select("h2").select("a").text();
                        String img_text =
                                gmbar.first().select("img[src]").attr("src");
                        String description = desc_last.last().
                                select("p").text();
                        try {
                            itemArrayList.add(new Item(title_text, img_text, description, "SELENGKAPNYA", link_text));
                        } catch (Exception e) {
                            Log.e("error_data", "run: " + e);
                        }
                    }
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                    Log.e("error_get", "run: " + e);
                } catch (Exception ex) {
                    Log.e("error_get", "run: " + ex);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        recyclerView = findViewById(R.id.recycler_view);

                        adapter_item = new ItemAdapter(itemArrayList);
                        Log.e("adapter_item", "run: " + itemArrayList);

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainTemplate.this);

                        recyclerView.setLayoutManager(layoutManager);

                        recyclerView.setAdapter(adapter_item);
                        recyclerView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("view", "onClick: " + v);

                            }
                        });
                        linearLayoutSumber.setVisibility(View.VISIBLE);
                    }
                });
                progressDialog.dismiss();
            }
        }).start();
    }
}
