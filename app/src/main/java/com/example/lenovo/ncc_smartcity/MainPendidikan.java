package com.example.lenovo.ncc_smartcity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainPendidikan extends AppCompatActivity {

    private List<String> listDaftarPendidikan = new ArrayList<>();
    private PendidikanAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pendidikan_main);

        adapter = new PendidikanAdapter(this, listDaftarPendidikan);
        listDaftarPendidikan.add("Daftar Sekolah Dasar");
        listDaftarPendidikan.add("Daftar Sekolah Menengah Pertama");
        listDaftarPendidikan.add("Daftar Sekolah Menengah Atas dan Kejuruan");
        listDaftarPendidikan.add("Pondok Pesantren");
        listDaftarPendidikan.add("Perguruan Tinggi");
        listDaftarPendidikan.add("Perpustakaan");

        RecyclerView recyclerView = findViewById(R.id.list_pendidikan);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }
}
