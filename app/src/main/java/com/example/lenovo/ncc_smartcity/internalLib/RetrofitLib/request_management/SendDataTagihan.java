package com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by USER on 21/06/2018.
 */

public interface SendDataTagihan {

    @Multipart
    @POST("show_report/showpdam/checkTagihan_x")
    Call<ResponseBody> get_pdam(@Part("token") RequestBody token,
                                   @Part("nosal") RequestBody nosal);


//    String JSONGETURL = "http://36.91.58.53:8088/kominfo/";
//    @GET("pbb.php")
//    Call<String> getPbb( @QueryMap Map<String, String> options);

    @Multipart
    @POST("bp2d/Bppdapi/get_pbb")
    Call<ResponseBody> get_pbb(@Part("nop") RequestBody nop,
                                @Part("nama_wp") RequestBody nama_wp);

}