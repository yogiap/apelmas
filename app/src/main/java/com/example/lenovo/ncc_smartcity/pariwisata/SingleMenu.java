package com.example.lenovo.ncc_smartcity.pariwisata;

import android.Manifest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.squareup.picasso.Picasso;

public class SingleMenu extends FragmentActivity {

    String jalan;
    String TAG = "SingleMenu";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pariwisata_single_menu);

        ImageView google = findViewById(R.id.location);
        final TextView nama = findViewById(R.id.nama);
        final TextView deskripsi = findViewById(R.id.deskripsi);
        final ImageView gambar = findViewById(R.id.gambar);
        TypedArray icons = getResources().obtainTypedArray(R.array.list_icon);
        final TextView keterangan = findViewById(R.id.keterangan);
        final TextView telepon = findViewById(R.id.telepon);
        TextView textView2 = findViewById(R.id.tv_pariwisata_list);

        Intent intent = getIntent();
        final int a = intent.getIntExtra("id", -1);
        final String b = intent.getStringExtra("selected");
        final String titlesEx = intent.getStringExtra("titles");
        final String alamatEx = intent.getStringExtra("alamat");
        final String deskripsiEx = intent.getStringExtra("deskripsi");
        final String teleponEx = intent.getStringExtra("telepon");
        final String gambarEx = intent.getStringExtra("gambar");
        nama.setText(titlesEx);
        deskripsi.setText(alamatEx);
        Picasso.with(SingleMenu.this).load(gambarEx).into(gambar);
        jalan = alamatEx;
        keterangan.setText(deskripsiEx);
        telepon.setText(teleponEx);


        if (Build.VERSION.SDK_INT < 19) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Log.e(TAG, "onCreate: " + policy);
        }

        if (ActivityCompat.checkSelfPermission(SingleMenu.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(SingleMenu.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e("TAG", "onMapReady: fail");
            requestPermissions();
        }

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=" + jalan));
                    Log.e(TAG, "onClick: " + intent);
                    startActivity(intent);

                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e);
                }
            }
        });


    }


    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
    }

    private void request_permission() {
        ActivityCompat.requestPermissions(SingleMenu.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        ActivityCompat.requestPermissions(SingleMenu.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
    }

}


